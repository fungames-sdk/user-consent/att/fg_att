# ATT

## Introduction

For iOS apps, App Tracking Transparency is required to collect user specific data.

## Integration Steps

1) **"Install"** or **"Upload"** FG ATT plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.